﻿using System;
using System.IO;
using System.Windows.Forms;
using Networking_Engine.Packets;
using Networking_Engine.Serializer;
using Networking_Engine.TCP;

namespace ChatClient
{
    public partial class Form1 : Form
    {
        private readonly Client _client;

        public Form1()
        {
            InitializeComponent();

            foreach (Control c in Controls)
                c.Enabled = false;

            Serializer.RegisterCustomType(new Person());

            _client = new Client(1024);
            Client.Debug = true;

            _client.OnConnected += _client_OnConnected;
            _client.OnPacketReceived += _client_OnPacketReceived;
            _client.OnPingUpdate += _client_OnPingUpdate;

            _client.Connect("localhost", 666);
        }

        void _client_OnPingUpdate(int currentPing)
        {
            Invoke(new Action(() => Text = @"Ping: " + currentPing));
        }

        void _client_OnPacketReceived(Packet packet, Client server)
        {
            if (packet.Header == 0x1)
            {
                var ar = (string[])packet.Args[2];

                richTextBox1.Invoke(new Action(() =>
                    richTextBox1.AppendText(ar[1] + " - " + packet.Args[1] + Environment.NewLine)));
            }
        }

        void _client_OnConnected(Client client, bool connected)
        {
            Invoke(new Action(() =>
            {
                Text = client.Statistics.Guid.ToString();

                foreach (Control c in Controls)
                    c.Enabled = connected;
            }));
        }

        void SendMessage(string message)
        {
            _client.Send(0x1, message);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SendMessage(textBox1.Text);
        }
    }

    public class Person : ISerializable
    {
        public string Name; // Name of the Person
        public int Age; // Age of the person

        public override string ToString()
        {
            return Name + " " + Age;
        }

        public void Write(BinaryWriter writer, object value)
        {
            var p = value as Person ?? new Person(); // If the value is null, just create a new one for the lulz, this is not needed

            writer.Write(p.Name); // Write Name
            writer.Write(p.Age); // Write Age
        }

        public object Read(BinaryReader reader)
        {
            // return the new Person, it is inportant we read in the same order as what we wrote
            return new Person
            {
                Name = reader.ReadString(), // Read Name
                Age = reader.ReadInt32() // Read Age
            };
        }
    }
}
