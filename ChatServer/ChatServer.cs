﻿using System;
using System.IO;
using Networking_Engine.Packets;
using Networking_Engine.Serializer;
using Networking_Engine.TCP;

namespace ChatServer
{
    class ChatServer
    {
        private static Server _server;

        static void Main()
        {
            Serializer.RegisterCustomType(new Person());

            _server = new Server(1024)
            {
                AllowRemoteDebuging = true
            };

            Client.Debug = true;

            _server.OnClientConnected += server_OnClientConnected;
            _server.OnClientDisconnected += server_OnClientDisconnected;
            _server.OnPacketReceived += server_OnPacketReceived;

            _server.Start(666, 1000);
        }

        static void server_OnPacketReceived(Packet packet, Client client, Server server)
        {
            SendMessage(client.Statistics.EndPoint.ToString(), (string)packet.Args[0]);
        }

        static void server_OnClientDisconnected(Client client, Client server)
        {
            SendMessage("Server", string.Format("{0} - Has left", client.Statistics.Guid));
        }

        static void server_OnClientConnected(Client client, Client server)
        {
            SendMessage("Server", string.Format("{0} - Has joined", client.Statistics.Guid));
        }

        private static void SendMessage(string sender, string message)
        {
            Console.WriteLine("Sender: {0} - Message {1}", sender, message);

            foreach (var c in _server.GetConnectedClients())
            {
                Console.WriteLine("Sending Packet, length: " + 
                    c.Send(0x1, sender, message,
                        new[] {"Hello", "Batman"}));
            }
        }
    }

    public class Person : ISerializable
    {
        public string Name; // Name of the Person
        public int Age; // Age of the person

        public void Write(BinaryWriter writer, object value)
        {
            var p = value as Person ?? new Person(); // If the value is null, just create a new one for the lulz, this is not needed

            writer.Write(p.Name); // Write Name
            writer.Write(p.Age); // Write Age
        }

        public object Read(BinaryReader reader)
        {
            // return the new Person, it is inportant we read in the same order as what we wrote
            return new Person
            {
                Name = reader.ReadString(), // Read Name
                Age = reader.ReadInt32() // Read Age
            };
        }
    }
}
