﻿using System.IO;
using System.IO.Compression;
using System.Text;

namespace Networking_Engine
{
    public class Compression
    {
        public static bool Enabled = false;

        public static byte[] Compress(byte[] bytes)
        {
            if (!Enabled)
                return bytes;

            using (var memory = new MemoryStream())
            {
                using (var gzip = new GZipStream(memory, CompressionMode.Compress))
                {
                    gzip.Write(bytes, 0, bytes.Length);
                }
                bytes = memory.ToArray();
            }
            return bytes;
        }

        public static byte[] Decompress(byte[] bytes)
        {
            if (!Enabled)
                return bytes;

            using (var stream = new GZipStream(new MemoryStream(bytes), CompressionMode.Decompress))
            {
                var buffer = new byte[1024];
                using (var memory = new MemoryStream())
                {
                    int count;
                    do
                    {
                        count = stream.Read(buffer, 0, 1024);
                        if (count > 0)
                            memory.Write(buffer, 0, count);
                    }
                    while (count > 0);

                    return memory.ToArray();
                }
            }
        }

        public static byte[] Compress(string value)
        {
            return Compress(Encoding.UTF8.GetBytes(value));
        }

        public static byte[] Decompress(string value)
        {
            return Decompress(Encoding.UTF8.GetBytes(value));
        }
    }
}