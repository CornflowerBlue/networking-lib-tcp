﻿using System;
using System.IO;
using System.Linq;

namespace Networking_Engine.Packets
{
    public class Packet
    {
        public object[] Args;
        public byte Header;
        internal byte OpHeader;

        public Packet()
        {
        }

        public Packet(byte header, params object[] args)
        {
            Header = header;
            Args = args;

            OpHeader = 0x0;
        }

        public override string ToString()
        {
            return string.Format("0x{0:x} - 0x{1:x}: {2}", OpHeader, Header, Args.Length);
        }

        public static Packet GetPacketFromBytes(BinaryReader br) // Use this to read the data from the packets
        {
            try
            {
                var length = br.ReadInt32();
                var cbytes = br.ReadBytes(length);
                var bytes = Compression.Decompress(cbytes);

                if (bytes.Length == 0) return null;

                using (var ms = new MemoryStream(bytes))
                using (var b = new BinaryReader(ms))
                {

                    var opHeader = b.ReadByte();
                    var header = b.ReadByte();
                    var argCount = b.ReadByte();

                    var packet = new Packet
                    {
                        OpHeader = opHeader,
                        Header = header,
                        Args = new object[argCount]
                    };

                    for (var i = 0; i < packet.Args.Length; i++)
                        packet.Args[i] = Serializer.Serializer.ReadValueData(b);

                    return packet;
                }
            }
            catch
            {
                return null;
            }
        }

        public static byte[] GetBytesFromPacket(Packet p)
        {
            return GetBytesFromPacket(p.OpHeader, p.Header, p.Args);
        }

        public static byte[] GetBytesFromPacket(byte opCommand, byte command, params object[] args)
        {
            if (args.Length > byte.MaxValue)
                throw new Exception("Arg count is too high, limit is " + byte.MaxValue);

            using (var ms = new MemoryStream())
            using (var bw = new BinaryWriter(ms))
            {
                bw.Write(opCommand); // Command
                bw.Write(command); // Command

                bw.Write((byte) args.Length); // Args count

                foreach (var p in args)
                    Serializer.Serializer.Write(bw, p); // Arg data

                var compressed = Compression.Compress(ms.ToArray()).ToList();
                compressed.InsertRange(0, BitConverter.GetBytes(compressed.Count));

                return compressed.ToArray();
            }
        }
    }
}