﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Networking_Engine.Serializer
{
    public static class Serializer
    {
        internal static Dictionary<Type, byte> TypeTable = new Dictionary<Type, byte>
        {
            { typeof    (bool),    0  },
            { typeof    (byte),    1  },
            { typeof   (short),    2  },
            { typeof   (float),    3  },
            { typeof     (int),    4  },
            { typeof    (uint),    5  },
            { typeof    (long),    6  },
            { typeof   (ulong),    7  },
            { typeof  (string),    8  },
            { typeof    (Guid),    9  },
            { typeof   (Array),   10  },
            { typeof  (UInt16),   11  },
            { typeof  (double),   12  },
            { typeof (decimal),   13  }
        };

        internal static Dictionary<Type, ISerializable> CustomTypes = new Dictionary<Type, ISerializable>();
        internal static Dictionary<Type, byte> CustomTypesCodes = new Dictionary<Type, byte>();

        public static bool RegisterCustomType(ISerializable type)
        {
            lock (CustomTypes)
            {
                var byteCode = (byte) (CustomTypes.Count + TypeTable.Count);
                CustomTypes.Add(type.GetType(), type);
                CustomTypesCodes.Add(type.GetType(), byteCode);
            }

            return true;
        }

        internal static byte TypeToByte(Type t)
        {
            if (t == null)
                return Byte.MaxValue;

            return TypeTable.ContainsKey(t) ? TypeTable[t] : CustomTypes.ContainsKey(t) ? CustomTypesCodes[t] : byte.MaxValue;
        }

        internal static Type ByteToType(byte b)
        {
            var rtn = TypeTable.Where(t => t.Value == b).Select(t => t.Key).FirstOrDefault() ??
                      CustomTypes.Where(t => CustomTypesCodes[t.Key] == b).Select(t => t.Key).FirstOrDefault();

            return rtn;
        }

        internal static void Write(BinaryWriter writer, object value, bool includeHeader = true)
        {
            var byteCode = TypeToByte(value.GetType());

            if (value.GetType().IsArray)
                byteCode = TypeToByte(typeof(Array));

            if (includeHeader)
                writer.Write(byteCode);

            switch (byteCode)
            {
                case 0:
                    writer.Write((bool)value);
                    return;
                case 1:
                    writer.Write((byte)value);
                    return;
                case 3:
                    writer.Write((float)value);
                    return;
                case 12: writer.Write((double)value);
                    return;
                case 13: writer.Write((decimal)value);
                    return;
                case 2:
                    writer.Write((Int16)value);
                    return;
                case 11:
                    writer.Write((UInt16)value);
                    return;
                case 4:
                    writer.Write((int)value);
                    return;
                case 5:
                    writer.Write((uint)value);
                    return;
                case 6:
                    writer.Write((long)value);
                    return;
                case 7:
                    writer.Write((ulong)value);
                    return;
                case 8:
                    writer.Write((string)value);
                    return;
                case 9:
                    writer.Write(((Guid)value).ToByteArray());
                    return;
                case 10:
                    var array = (Array) value;
                    if(array.Rank > 1) throw new Exception("Multidimensional arrays are not supported");
                    writer.Write(array.Length);
                    writer.Write(TypeToByte(array.GetValue(0).GetType()));

                    for (var i = 0; i < array.Length; i++)
                        Write(writer, array.GetValue(i), false);
                    return;
            }

            if (CustomTypes.ContainsKey(value.GetType()))
                CustomTypes[value.GetType()].Write(writer, value);
        }

        internal static object ReadValueData(BinaryReader reader, byte valueType = 0xFF)
        {
            var typeId = valueType == 0xFF ? reader.ReadByte() : valueType;
            if (typeId == 0xFF) return null;

            switch (typeId)
            {
                case 0: return reader.ReadBoolean();
                case 1: return reader.ReadByte();
                case 3: return reader.ReadSingle();
                case 12:return reader.ReadDouble();
                case 13:return reader.ReadDecimal();
                case 2: return reader.ReadInt16();
                case 11:return reader.ReadUInt16();
                case 4: return reader.ReadInt32();
                case 5: return reader.ReadUInt32();
                case 6: return reader.ReadInt64();
                case 7: return reader.ReadUInt64();
                case 8: return reader.ReadString();
                case 9: return new Guid(reader.ReadBytes(16));
                case 10:
                    var length = reader.ReadInt32();
                    var type = reader.ReadByte();
                    var lst = Array.CreateInstance(ByteToType(type), length);

                    for (var i = 0; i < length; i++)
                    {
                        var data = ReadValueData(reader, type);
                        lst.SetValue(Convert.ChangeType(data, ByteToType(type)), i);
                    }

                    return lst;
            }

            return CustomTypes.ContainsKey(ByteToType(typeId)) ? CustomTypes[ByteToType(typeId)].Read(reader) : null;
        }
    }

    public interface ISerializable
    {
        void Write(BinaryWriter writer, object value);
        object Read(BinaryReader reader);
    }
}
