﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using Networking_Engine.Packets;

namespace Networking_Engine.TCP
{
    public delegate void Connected(Client client, bool connected);
    public delegate void ClientData(Packet packet, Client server);
    public delegate void DataSend(byte[] bytes);
    public delegate void Ping(int currentPing);

    public sealed class Client : IDisposable
    {
        // Events
        public event Connected OnConnected;
        public event ClientData OnPacketReceived;
        public event Ping OnPingUpdate;
        internal event ClientData OnInternalPacketReceived;
        internal event DataSend OnDataSend;
        internal event DataSend OnDataReceive;

        public static bool Debug = false;

        private readonly int _buffer;
        private readonly Queue<byte[]> _sendQueue = new Queue<byte[]>();
        internal StateObject State;
        internal Socket Socket;
        private bool _hasDisconnected;
        private bool _processing;

        public bool Connected { get { return Socket != null && Socket.Connected; } }

        public bool Debugger;
        public string DebuggerPassword;

        public Stats Statistics = new Stats();

        public Client(int buffer)
        {
            _buffer = buffer;
        }
        internal Client(Socket s, int buffer)
        {
            Socket = s;
            _buffer = buffer;

            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, true);
            s.NoDelay = true;
        }

        public int Send(Packet packet)
        {
            return Send(packet.OpHeader, packet.Header, packet.Args);
        }
        public int Send(byte header, params object[] param)
        {
            return Send(0x0, header, param);
        }
        internal int Send(byte opCommand, byte header, params object[] param)
        {
            if (Debug && opCommand == 0x0)
                Console.WriteLine("Sending packet 0x{0}", header.ToString("X"));

            var bytes = Packet.GetBytesFromPacket(opCommand, header, param);

            if (bytes == null) return 0;
            Send(bytes);

            return bytes.Length;
        }
        internal void Send(byte[] data)
        {
            if (!Connected)
                return;

            if (Debug)
                Console.WriteLine("Sent packet, length: " + data.Length);

            if (OnDataSend != null)
                OnDataSend(data);

            lock (_sendQueue)
            {
                _sendQueue.Enqueue(data);

                if (!_processing)
                    HandleSendQueue();
            }
        }

        private void HandleSendQueue()
        {
            if (!Connected)
                return;

            _processing = true;

            var bytes = new List<byte>();

            lock (_sendQueue)
            {
                while (_sendQueue.Count > 0)
                    bytes.AddRange(_sendQueue.Dequeue());
            }

            try
            {
                if (Debug)
                    Console.WriteLine("Sending packet from queue, length: " + bytes.Count);
                Socket.BeginSend(bytes.ToArray(), 0, bytes.Count, 0, SendCallback, null);
            }
            catch (Exception e)
            {
                Console.WriteLine("#4 - " + e + " - " + e.Message);
                Disconnect();
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket.EndSend(ar);

                if (_sendQueue.Count > 0)
                    HandleSendQueue();
                else
                    _processing = false;
            }
            catch (Exception e)
            {
                Console.WriteLine("#5 - " + e + " - " + e.Message);
                Disconnect();
            }
        }

        public void ReadCallback(IAsyncResult ar)
        {
            try
            {
                var bytesRead = Socket.EndReceive(ar);

                if (bytesRead > 0)
                {
                    if (Debug)
                        Console.WriteLine("Got data: " + bytesRead);

                    var bytes = new byte[bytesRead];
                    Buffer.BlockCopy(State.Buffer, 0, bytes, 0, bytesRead);

                    if (OnDataReceive != null)
                        OnDataReceive.Invoke(bytes);

                    if (bytesRead >= _buffer || State.BufferCache.Count > 0)
                    {
                        State.BufferCache.AddRange(bytes);
                        bytes = State.BufferCache.ToArray();
                    }

                    State.BytesRead += bytesRead;

                    var expectedLength = BitConverter.ToUInt32(bytes, 0);
                    var bytesReadCache = State.BytesRead;

                    // Need to get mor data
                    if (State.BytesRead >= expectedLength + 4)
                    {
                        var bytesUsed = HandleReceiveData(bytes);

                        State.BytesRead = 0;
                        State.BufferCache.Clear();

                        if (bytesUsed > 0 && bytesReadCache > bytesUsed)
                        {
                            Console.WriteLine("There are {0} unused bytes", bytesReadCache - bytesUsed);
                            var nextBytes = new byte[bytesReadCache - bytesUsed];
                            Buffer.BlockCopy(bytes, (int) bytesUsed, nextBytes, 0, nextBytes.Length);
                            State.BufferCache.AddRange(nextBytes);

                            State.BytesRead += nextBytes.Length;
                        }
                    }
                }

                Socket.BeginReceive(State.Buffer, 0, _buffer, 0, ReadCallback, null);
            }
            catch (Exception ex)
            {
                if(Debug)
                    Console.WriteLine("#C2 - " + ex + " - " + ex.Message);
                Disconnect();
            }
        }

        private long HandleReceiveData(byte[] bytes)
        {
            long index = 0;
            try
            {
                var packets = new List<Packet>();

                using (var ms = new MemoryStream(bytes))
                using (var br = new BinaryReader(ms))
                {
                    var newPacket = Packet.GetPacketFromBytes(br);

                    while (newPacket != null)
                    {
                        packets.Add(newPacket);

                        if (ms.Position == bytes.Length)
                            break;

                        var brPos = br.BaseStream.Position;
                        newPacket = Packet.GetPacketFromBytes(br);

                        if (newPacket == null)
                            br.BaseStream.Position = brPos;
                    }

                    index = br.BaseStream.Position;
                }

                if (Debug)
                    Console.WriteLine("I got {0} packets from {1} bytes, raising events for them all now", packets.Count,
                        bytes.Length);

                foreach (var p in packets)
                {
                    if (p.OpHeader != 0x0) // Internal Commands but not debugger commands
                    {
                        if (p.OpHeader == 0x1 && p.Header == 0x0)
                        {
                            Send(0x1, 0x1); // Ping response
                            Statistics.Ping = (int)p.Args[0]; // Ping sync, Server -> Client ping

                            try
                            {
                                if(OnPingUpdate != null)
                                    OnPingUpdate.Invoke(Statistics.Ping);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("#73 - " + e + " - " + e.Message);
                            }
                        }
                        else if (p.OpHeader == 0x2 && p.Header == 0x0) // Connect handshake, and guid sync
                        {
                            Statistics.Guid = (Guid)p.Args[0];

                            if (Debugger)
                            {
                                Send(0x2, 0x2, DebuggerPassword);
                            }
                            else
                            {
                                Send(0x2, 0x1); // This is the server call back to tell the server I have connected
                            }

                            // Raise Join Event
                            try
                            {
                                if (OnConnected != null)
                                    OnConnected.Invoke(this, true);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("#72 - " + e + " - " + e.Message);
                            }
                        }
                        else if (p.OpHeader == 0x11 && Debugger)
                        {
                            try
                            {
                                if (OnPacketReceived != null)
                                    OnPacketReceived.Invoke(p, this);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("#71d - " + e + " - " + e.Message);
                            }
                        }

                        if (OnInternalPacketReceived != null)
                            OnInternalPacketReceived.Invoke(p, this);
                    }
                    else
                    {
                        if (Debug)
                            Console.WriteLine(p.ToString());

                        try
                        {
                            if (OnPacketReceived != null)
                                OnPacketReceived.Invoke(p, this);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("#71 - " + e + " - " + e.Message);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (Debug)
                    Console.WriteLine("#70 - " + e + " - " + e.Message);
                Disconnect();
            }

            return index;
        }

        // Client stuff
        public bool Connect(string host, ushort port)
        {
            try
            {
                var endpoint = new IPEndPoint(GetAddress(host), port);
                Statistics.EndPoint = endpoint;

                Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                Socket.BeginConnect(endpoint, ConnectCallback, null);

                Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, true);
                Socket.NoDelay = true;


                var t = new Timer(2000);
                t.Elapsed += HandShake;
                t.Start();
            }
            catch
            {
                return false;
            }

            return true;
        }

        void HandShake(object sender, ElapsedEventArgs e)
        {
            if (!Socket.Connected) return;
            Send(0x1, 0x5);
        }

        private IPAddress GetAddress(string host)
        {
            var hosts = Dns.GetHostAddresses(host);

            return hosts.FirstOrDefault(h => h.AddressFamily == AddressFamily.InterNetwork);
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket.EndConnect(ar);

                State = new StateObject {Client = this, Buffer = new byte[_buffer]};

                Socket.BeginReceive(State.Buffer, 0, _buffer, 0, ReadCallback, State);
            }
            catch
            {
                Disconnect();
            }
        }

        public void Disconnect()
        {
            if(Debug)
                Console.WriteLine(Statistics.Guid + " - Disconnected");

            if (_hasDisconnected) return;
            _hasDisconnected = true;

            if (OnConnected != null)
                OnConnected.Invoke(this, false);

            _sendQueue.Clear();

            if (Socket.Connected)
            {
                Socket.Shutdown(SocketShutdown.Both);
                Socket.Close();
                //Socket.BeginDisconnect(true, ar => Socket.EndDisconnect(ar), null);
            }

            Statistics.EndPoint = null;
        }

        public override string ToString()
        {
            return Statistics.Guid.ToString();
        }

        public void Dispose()
        {
            Socket.Dispose();
        }
    }

    public class Stats
    {
        public IPEndPoint EndPoint;
        public Guid Guid;
        public int Ping = -1;
    }
}