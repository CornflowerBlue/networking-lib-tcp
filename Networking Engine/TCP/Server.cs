﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using Networking_Engine.Packets;
using Timer = System.Timers.Timer;

namespace Networking_Engine.TCP
{
    public delegate void ClientConnect(Client client, Client server);
    public delegate void Data(Packet packet, Client client, Server server);

    public class Server
    {
        private readonly ManualResetEvent _allDone = new ManualResetEvent(false);
        private readonly int _buffer;

        private readonly List<Client> _connectedClients = new List<Client>();
        private readonly Dictionary<Client, TimeSpan> _handshakeTimer = new Dictionary<Client, TimeSpan>();

        public int MaxConnections = -1;
        private Client _server;

        public bool AllowRemoteDebuging;
        public string RemoteDebuggingPassword = "";
        internal List<Client> DebuggerClients = new List<Client>();

        public Server(int buffer)
        {
            _buffer = buffer;
        }

        public event ClientConnect OnClientConnected;
        public event ClientConnect OnClientDisconnected;
        public event Data OnPacketReceived;

        public void Start(ushort port, int handshakeTickRateMs = 5000)
        {
            // HandShake
            var timer = new Timer(handshakeTickRateMs);
            timer.Elapsed += HandShake;
            timer.Start();

            new Thread(() =>
            {
                _server = new Client(new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp),
                    _buffer);

                _server.Socket.Bind(new IPEndPoint(IPAddress.Any, port));
                _server.Socket.Listen(16);

                while (true)
                {
                    try
                    {
                        _allDone.Reset();

                        _server.Socket.BeginAccept(AcceptCallback, _server.Socket);

                        _allDone.WaitOne();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("#3 - " + e + " - " + e.Message);
                        Thread.Sleep(10);
                    }
                }
// ReSharper disable once FunctionNeverReturns
            }).Start();
        }

        private void HandShake(object sender, ElapsedEventArgs e)
        {
            lock (_connectedClients)
            {
                var clients = _connectedClients;
                clients.AddRange(DebuggerClients);

                foreach (var c in clients)
                {
                    try
                    {
                        _handshakeTimer[c] = DateTime.Now.TimeOfDay;
                        c.Send(0x1, 0x0, c.Statistics.Ping);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("#2 - " + ex + " - " + ex.Message);
                        c.Disconnect();
                    }
                }
            }
        }

        private void _server_OnPacketReceived(Packet packet, Client client)
        {
            if (OnPacketReceived != null)
                OnPacketReceived.Invoke(packet, client, this);

            foreach (var debugger in DebuggerClients)
                debugger.Send(0x11, packet.Header, packet.Args);
        }

        private void client_OnInternalPacketReceived(Packet packet, Client client)
        {
            if (packet.OpHeader == 0x1 && packet.Header == 0x1)
            {
                client.Statistics.Ping = (int) (DateTime.Now.TimeOfDay - _handshakeTimer[client]).TotalMilliseconds;
            }
            else if (packet.OpHeader == 0x2)
            {
                if (packet.Header == 0x1)
                {
                    if (_connectedClients.Contains(client))
                    {
                        client.Disconnect();
                        return;
                    }

                    // Raise events
                    client_OnConnected(client, true);
                    if (OnClientConnected != null)
                        OnClientConnected.Invoke(client, _server);
                }
                else if (packet.Header == 0x2) // Client is debuger
                {
                    if (!AllowRemoteDebuging || (string) packet.Args[0] != RemoteDebuggingPassword)
                    {
                        client.Send(0x10, 0);
                        client.Disconnect();
                    }
                    else
                    {
                        lock (DebuggerClients)
                        {
                            if (DebuggerClients.Contains(client))
                            {
                                client.Disconnect();
                                return;
                            }
                            DebuggerClients.Add(client);
                        }

                        client.Send(0x10, 1);
                        Console.WriteLine("Debugger connected");
                    }
                }
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                _allDone.Set();

                var serverSock = (Socket) ar.AsyncState;
                var clientSock = serverSock.EndAccept(ar);

                if (MaxConnections > 0 && _connectedClients.Count > MaxConnections)
                {
                    serverSock.Disconnect(true);
                    return;
                }

                var client = new Client(clientSock, _buffer);
                var state = new StateObject {Client = client, Buffer = new byte[_buffer]};
                client.State = state;

                client.OnConnected += client_OnConnected;
                client.OnInternalPacketReceived += client_OnInternalPacketReceived;
                client.OnPacketReceived += _server_OnPacketReceived;

                client.Socket.BeginReceive(state.Buffer, 0, _buffer, 0, client.ReadCallback, state);

                client.Statistics.EndPoint = (IPEndPoint) client.Socket.RemoteEndPoint;
                client.Statistics.Guid = Guid.NewGuid();

                client.Send(0x2, 0x0, client.Statistics.Guid);
            }
            catch (Exception e)
            {
                Console.WriteLine("#1 - " + e + " - " + e.Message);
            }
        }

        private void client_OnConnected(Client client, bool connected)
        {
            if (connected)
            {
                lock (_connectedClients)
                {
                    _connectedClients.Add(client);
                }
                lock (_handshakeTimer)
                {
                    _handshakeTimer.Add(client, TimeSpan.Zero);
                }
            }

            if (!connected)
            {
                lock (DebuggerClients)
                {
                    if (DebuggerClients.Contains(client))
                    {
                        DebuggerClients.Remove(client);

                        lock (_handshakeTimer)
                        {
                            _handshakeTimer.Remove(client);
                        }
                        return;
                    }
                }

                
                lock (_connectedClients)
                {
                    lock (_handshakeTimer)
                    {
                        _handshakeTimer.Remove(client);
                    }

                    if (_connectedClients.Contains(client))
                    {
                        _connectedClients.Remove(client);
                    }
                    else return;

                    if (OnClientDisconnected != null)
                    {
                        OnClientDisconnected.Invoke(client, _server);
                    }
                }
            }
        }

        public Client[] GetConnectedClients()
        {
            return _connectedClients.ToArray();
        }
    }

    public class StateObject
    {
        public byte[] Buffer;

        // Real buffer for real boys
        public List<Byte> BufferCache = new List<byte>();
        public long BytesRead;
        public Client Client;
    }
}